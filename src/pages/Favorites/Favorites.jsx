import style from './favorites.module.scss'
import Container from '../../components/Container';
import ProductList from '../../components/ProductList';
import { useSelector } from "react-redux";

export function Favorites() {


    const favorites = useSelector(state => state.favorites.favorites)
    const loading = useSelector(state => state.loading.loading)
    const error = useSelector(state => state.error.error)

    if (error) {
        return <p>Відбулась непередбаченна помилка при завантаженні, спробуйте оновити сторінку</p>;
    }

    return (
        <Container>
            {!loading
                ? favorites.length
                    ? <ProductList products={favorites} type={'buy'} />
                    : <p className={style.empty}>Ви ще нічого не додали в обране</p>
                : <p className={style.empty}>Йде завантаження...</p>}
        </Container>
    )
}