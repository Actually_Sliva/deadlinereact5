import ProductItem from "../ProductItem"
import style from './productList.module.scss'
import PropTypes from 'prop-types';

export default function ProductList({ type, products, productAmount }) {

    return (
        <div className={style.productList}>
            {products.map(product =>
                <ProductItem
                    key={product.artNum}
                    product={product}
                    type={type}
                    productAmount={productAmount} />
            )}
        </div>
    )
}

ProductList.propTypes = {
    products: PropTypes.array,
    type: PropTypes.string,
    productAmount: PropTypes.bool,
}